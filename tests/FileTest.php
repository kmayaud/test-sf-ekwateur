<?php

namespace App\tests;

use App\File;
use App\JsonFile;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \App\File
 */
class FileTest extends TestCase
{
    /**
     * @covers ::buildPromoCodeList
     * @dataProvider dataProvider
     */
    public function testBuildPromoCodeList(array $detailedCode, array $offersForDetailedCode)
    {
        $file = new JsonFile();
        $dataToFormat = $file->buildPromoCodeList($detailedCode, $offersForDetailedCode);
        $this->assertArrayHasKey('promoCode', $dataToFormat, 'The array must have promoCode key');
        $this->assertArrayHasKey('endDate', $dataToFormat, 'The array must have endDate key');
        $this->assertArrayHasKey('discountValue', $dataToFormat, 'The array must have discountValue key');
        $this->assertArrayHasKey('compatibleOfferList', $dataToFormat, 'The array must have compatibleOfferList key');
    }

    public function dataProvider() : array
    {
        return [
            [
                'detailedCode' => [
                    'code' => "EKWA_WELCOME",
                    'discountValue' => 2,
                    'endDate' => "2019-10-04"
                ],
                'offersForDetailedCode' => [
                    [
                        'name'=> "EKWAG2000",
                        'type'=> "GAS",
                    ],
                    [
                        'name'=> "EKWAG3000",
                        'type'=> "GAS",
                    ],
                    [
                        'name'=> "EKWAG2000",
                        'type'=> "ELECTRICITY",
                    ],
                ],
            ]
        ];
    }
}
