<?php

namespace App;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ApiStrategy implements IDataStrategy
{
    /**
     * @param array $settings
     * @return array
     */
    public function getData(array $settings): array
    {
        try {
            return [
                'promoCodeList' => $this->getPromoCodeList($settings['promoCodeListUrl']),
                'offerList' => $this->getOfferList($settings['offerListUrl']),
            ];
        } catch (ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | TransportExceptionInterface $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * @param string $url
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getPromoCodeList(string $url): array
    {
        return $this->getDataFromApi($url);
    }

    /**
     * @param string $url
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getOfferList(string $url): array
    {
        return $this->getDataFromApi($url);
    }


    /**
     * @param string $url
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getDataFromApi(string $url): array
    {
        return HttpClient::create()->request('GET', $url)->toArray();
    }
}