<?php

namespace App;

interface IDataStrategy
{
    public function getData(array $settings) : array;
}