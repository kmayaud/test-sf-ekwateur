<?php

namespace App;

class JsonFile extends File
{
    private string $name;
    private array $data;
    const TYPE = 'json';
    const DESTINATION_DIR = 'public/jsonFiles/';

    /**
     * Creates json file
     * @param array $data
     * @return File
     */
    public function createJsonFile(array $data) : File
    {
        $dataToFormat = $this->buildPromoCodeList($data['detailedCode'], $data['offersForDetailedCode']);
        $this->setName(date('Ymdhis'). '-ekwateur-test' . $this->getExtension());
        $this->setData($dataToFormat);
        file_put_contents(self::DESTINATION_DIR . $this->getName(), json_encode($dataToFormat, true));
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return '.'.self::TYPE;
    }
}