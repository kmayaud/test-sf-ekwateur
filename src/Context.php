<?php

namespace App;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Context
{
    /**
     * @var IDataStrategy
     */
    private IDataStrategy $strategy;

    /**
     * @param IDataStrategy $strategy
     */
    public function __construct(IDataStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function retrieveData($settings) : array
    {
        return $this->strategy->getData($settings);
    }


}