<?php

namespace App;

Abstract class File
{
    private string $name;
    private array $data;
    private array $type;

    /**
     * Build an array with prepared data
     * @param array $detailedCode
     * @param array $offersForDetailedCode
     * @return array
     */
    public function buildPromoCodeList(array $detailedCode, array $offersForDetailedCode) : array
    {
        return [
            'promoCode' => $detailedCode['code'],
            'endDate'=> $detailedCode['endDate'],
            'discountValue'=> number_format($detailedCode['discountValue'], 1),
            'compatibleOfferList' => $offersForDetailedCode
        ];
    }
}