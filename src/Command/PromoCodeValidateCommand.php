<?php

namespace App\Command;

use App\FileFactory;
use App\ApiStrategy;
use App\Context;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Yaml\Exception\ExceptionInterface;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    protected static $defaultDescription = 'Generates a JSON file with valid promo codes.';

    const EKWATEST_API = 'https://601025826c21e10017050013.mockapi.io/ekwatest';
    const PROMO_CODE_LIST_URL = self::EKWATEST_API . '/promoCodeList';
    const OFFER_LIST_URL = self::EKWATEST_API . '/offerList';
    private FileFactory $fileFactory;

    public function __construct(string $name = null, FileFactory $fileFactory)
    {
        parent::__construct($name);
        $this->fileFactory = $fileFactory;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::REQUIRED, 'Promo code to test.')
            ->setHelp('Test a promo code to see his validity.')
            ->setDescription('Check a promo code and compare it with an offer list (who contains promo codes).
             If it\'s match and the promo code has and end date later than today, the command will create a json file.')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            // Retrieve data with ApiStrategy.
            $settings = [
                'promoCodeListUrl' => self::PROMO_CODE_LIST_URL,
                'offerListUrl' => self::OFFER_LIST_URL
            ];
            $context = new Context(new ApiStrategy());
            try{
                $data = $context->retrieveData($settings);
            } catch (\Exception $exception){
                $io->error("Error when retrieve data.");
                return Command::FAILURE;
            }

            // data processing
            $jsonDataToBuild = [
                'detailedCode' => $this->validPromoCode($arg1, $data['promoCodeList']),
                'offersForDetailedCode' => $this->getOffersMatchCode($arg1, $data['offerList']),
            ];

            //A file will be create if $jsonDataToBuild['detailedCode'] and $jsonDataToBuild['offersForDetailedCode'] are full.
            if (!empty($jsonDataToBuild['detailedCode']) && !empty($jsonDataToBuild['offersForDetailedCode'])) {
                try {
                    $jsonFile = $this->fileFactory->executeCreationFile('json', $jsonDataToBuild);
                    $io->success(sprintf('You have created a new file named: %s', $jsonFile->getName() ?? 'default'));
                    return Command::SUCCESS;
                } catch (\Exception $exception) {
                    $io->error($exception->getMessage());
                    return Command::FAILURE;
                }
            } else {
                $io->error('There are not offers that correspond to promoCode, the promo code is not valid or has a typo.');
                return Command::FAILURE;
            }
        }
        return Command::FAILURE;
    }

    /**
     * Return details from code from list if tested code is valid.
     * @param string $testedCode
     * @param array $promoCodeList
     * @return array
     */
    public function validPromoCode(string $testedCode, array $promoCodeList) : array
    {
        foreach ($promoCodeList as $codeDetail) {
            if ($codeDetail['code'] === $testedCode && $codeDetail['endDate'] > date('now')) {
                return $codeDetail;
            }
        }
        return [];
    }


    /**
     * Return offers that match with promo code that are in valid promo code list
     * @param string $promoCode
     * @param array $offerList
     * @return array
     */
    private function getOffersMatchCode(string $promoCode, array $offerList) : array
    {
        $offers = [];
        foreach ($offerList as $offer) {
            if (is_array($offer['validPromoCodeList']) && in_array($promoCode, $offer['validPromoCodeList'])) {
                $offers[] = ['name' => $offer['offerName'], 'type' => $offer['offerType']];
            }
        }
        return $offers;
    }
}
