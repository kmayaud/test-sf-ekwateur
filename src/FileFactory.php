<?php

namespace App;

use Symfony\Component\Config\Definition\Exception\Exception;

class FileFactory
{
    /**
     * Choose type of file to create.
     * @param string $type
     * @param array $data
     * @return File
     */
    public function executeCreationFile(string $type, array $data): ?File
    {
        switch ($type) {
            case JsonFile::TYPE:
                $jsonFile = new JsonFile($data);
                return $jsonFile->createJsonFile($data);
            default:
                throw new Exception("File type is not yet supported.");
        }
    }
}